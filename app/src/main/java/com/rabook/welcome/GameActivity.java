package com.rabook.welcome;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.GridView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;

public class GameActivity extends AppCompatActivity {

    private SharedPreferences mSharedPreferences;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        mSharedPreferences = getSharedPreferences("donkey", MODE_PRIVATE);
        GridView gridview = findViewById(R.id.gridview);
        gridview.setOnTouchListener((v, event) -> event.getAction() == MotionEvent.ACTION_MOVE);
        ImageAdapter imageAdapter = new ImageAdapter(this);
        imageAdapter.setGameEndListener(count -> {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putInt("count", count);
            editor.apply();
            PlayAgainDialog playAgainDialog = new PlayAgainDialog(GameActivity.this, count, true);
            playAgainDialog.setOnClickListener((dialogInterface, i) -> {
                startActivity(new Intent(GameActivity.this, GameActivity.class));
                finish();
                dialogInterface.cancel();
            });
            playAgainDialog.show();
        });
        gridview.setAdapter(imageAdapter);
    }
}
