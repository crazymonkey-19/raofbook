package com.rabook.welcome;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayAgainDialog extends Dialog {

    private OnClickListener onClickListener;
    private int rating;
    private boolean type;

    @BindView(R.id.btnOk)
    ImageView btnOk;
    @BindView(R.id.txtScoreDialog)
    TextView txtScoreDialog;

    public PlayAgainDialog(@NonNull Context context, int rating, boolean type) {
        super(context, R.style.DialogTheme);
        this.rating = rating;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_play_again);
        ButterKnife.bind(this);
        if (type) {
            btnOk.setVisibility(View.VISIBLE);
        }
        txtScoreDialog.setText(getContext().getResources().getString(R.string.score, rating));
    }

    @OnClick(R.id.btnOk)
    void onClickOk() {
        if (onClickListener != null) {
            onClickListener.onClick(this, 0);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
