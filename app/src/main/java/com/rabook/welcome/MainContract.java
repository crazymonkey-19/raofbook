package com.rabook.welcome;

import android.net.Uri;

import com.rabook.welcome.presenter.basepresenter.MvpPresenter;
import com.rabook.welcome.presenter.basepresenter.MvpView;

public interface MainContract {

    interface View extends MvpView {
        void initFacebook();

        void initVebView(String uri);
    }

    interface Presenter extends MvpPresenter<View> {
        void setupDeepLink(Uri targetUri, String CountryCode);

        void cachingUrl(String url);

        void openWebViewWhenUrlExist();

        void getUrl(String countryCode, String key);
    }

}
