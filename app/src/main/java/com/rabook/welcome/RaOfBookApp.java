package com.rabook.welcome;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RaOfBookApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static RaOfBookApi getApi() {
        Retrofit retrofitRefresh = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofitRefresh.create(RaOfBookApi.class);
    }
}