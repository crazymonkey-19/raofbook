package com.rabook.welcome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.onesignal.OneSignal;
import com.rabook.welcome.presenter.MainPresenter;

import java.util.Objects;

import bolts.AppLinks;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainPresenter mMainPresenter;

    @BindView(R.id.webView)
    WebView webView;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mMainPresenter = new MainPresenter();
        mMainPresenter.attachView(this);
        mMainPresenter.viewIsReady();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        mSharedPreferences = getSharedPreferences("donkey", MODE_PRIVATE);
    }

    @OnClick(R.id.btnPlay)
    void onClickPlay() {
        String newUrl = mSharedPreferences.getString("new_url", "");
        if (!Objects.equals(newUrl, "")) {
            initVebView(newUrl);
        } else {
            startActivity(new Intent(this, DemoActivity.class));
        }
    }

    @OnClick(R.id.btnDemo)
    void onClickDemo() {
        startActivity(new Intent(this, DemoActivity.class));
    }

    @Override
    public void initVebView(String uri) {
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl(uri);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mMainPresenter.cachingUrl(url);
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (webView.getVisibility() == View.VISIBLE) {
            webView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void initFacebook() {
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (tm != null) {
            mMainPresenter.setupDeepLink(AppLinks.getTargetUrlFromInboundIntent(this, getIntent()),
                    tm.getNetworkCountryIso());
        } else {
            mMainPresenter.setupDeepLink(AppLinks.getTargetUrlFromInboundIntent(this, getIntent()),
                    "XX");
        }
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mMainPresenter != null) {
            mMainPresenter.detachView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMainPresenter != null) {
            mMainPresenter.destroy();
        }
    }
}

