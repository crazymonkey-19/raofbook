package com.rabook.welcome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class DemoActivity extends AppCompatActivity {

    private static final String KEY_SCORE = "count";
    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        ButterKnife.bind(this);
        mSharedPreferences = getSharedPreferences("donkey", Context.MODE_PRIVATE);
    }

    @OnClick(R.id.btnPlayGame)
    void onClickPlayGame() {
        startActivity(new Intent(this, GameActivity.class));
    }

    @OnClick(R.id.btnShowScore)
    void onClickShowScore() {
        if (mSharedPreferences != null) {
            openDialog(mSharedPreferences.getInt(KEY_SCORE, 0));
        }
    }

    @OnClick(R.id.btnLogout)
    void onClickLogout() {
        onBackPressed();
    }

    private void openDialog(int score) {
        PlayAgainDialog playAgainDialog = new PlayAgainDialog(this, score, false);
        Objects.requireNonNull(playAgainDialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        playAgainDialog.setOnClickListener((dialog, which) -> {
            playAgainDialog.cancel();
        });
        playAgainDialog.setCancelable(true);
        playAgainDialog.show();
    }
}
