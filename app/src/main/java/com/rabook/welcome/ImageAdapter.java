package com.rabook.welcome;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private Integer[] pieces;
    private List<ImageView> imageViews;
    private int piece_up = -1;
    private int mCountOfOk;
    private int mCount;
    private GameEndListener mGameEndListener;

    public void setGameEndListener(GameEndListener mGameEndListener) {
        this.mGameEndListener = mGameEndListener;
    }

    private Timer timer = new Timer();

    ImageAdapter(Context c) {
        mContext = c;
        List<Integer> ipieces = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            ipieces.add(i);
            ipieces.add(i);
        }
        Collections.shuffle(ipieces);
        pieces = ipieces.toArray(new Integer[0]);
        _createImageViews();
    }

    private void _createImageViews() {
        imageViews = new ArrayList<>();
        for (int position = 0; position < getCount(); position++) {
            ImageView imageView;
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(280, 280));
            imageView.setPadding(2, 10, 2, 10);
            imageView.setImageResource(R.drawable.card_item);
            imageViews.add(imageView);
            installClick(position);
        }
    }

    public int getCount() {
        return 12; //mThumbIds.length;
    }

    public Object getItem(int position) {
        return imageViews.get(position);
    }

    public long getItemId(int position) {
        return pieces[position].longValue();
    }

    // create a new ImageView for each item referenced by the Adapter
    public synchronized View getView(int position, View convertView, ViewGroup parent) {
        return imageViews.get(position);
    }

    void installClick(int position) {
        final ImageAdapter self = this;
        ImageView imageView = imageViews.get(position);
        imageView.setOnClickListener(v -> {
            int pos = imageViews.indexOf(v);
            show(pos);
            if (piece_up == -1 || piece_up == pos) {
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        mCount++;
                    }
                }, 1000, 500);
                // first click
                piece_up = pos;
            } else {
                // second click
                if (pieces[pos].equals(pieces[piece_up])) {
                    mCountOfOk++;
                    if (mCountOfOk == 6) {
                        timer.cancel();
                        mCountOfOk = 0;
                        if (mGameEndListener != null) {
                            mGameEndListener.onGameEnded(mCount);
                        }
                    }
                    // remove click handler
                    removeClick(pos);
                    removeClick(piece_up);
                } else {
                    // try again
                    int[] aux = {piece_up, pos};
                    SleepHide update = new SleepHide(mContext, self, aux);
                    Handler mHandler = new Handler();
                    mHandler.postDelayed(update, 300);
                }
                piece_up = -1;
            }
        });
    }

    void removeClick(int position) {
        ImageView aux;
        aux = imageViews.get(position);
        aux.setOnClickListener(null);
    }

    void hide(int position) {
        ImageView img;
        img = imageViews.get(position);
        img.setImageResource(R.drawable.card_item);
    }

    private void show(int position) {
        ImageView img;
        img = imageViews.get(position);
        int piece = pieces[position];
        img.setImageResource(mThumbIds[piece]);
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.papirus,
            R.drawable.lopata,
            R.drawable.gold,
            R.drawable.key,
            R.drawable.book,
            R.drawable.paravon
    };

    public interface GameEndListener {
        void onGameEnded(int count);
    }
}
