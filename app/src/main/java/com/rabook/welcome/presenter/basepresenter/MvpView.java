package com.rabook.welcome.presenter.basepresenter;


import android.content.Context;

public interface MvpView {

    Context getAppContext();

}
