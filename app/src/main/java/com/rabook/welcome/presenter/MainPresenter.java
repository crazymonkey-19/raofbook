package com.rabook.welcome.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.rabook.welcome.MainContract;
import com.rabook.welcome.RaOfBookApp;
import com.rabook.welcome.presenter.basepresenter.BasePresenter;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.TELEPHONY_SERVICE;

public class MainPresenter extends BasePresenter<MainContract.View>
        implements MainContract.Presenter {

    private SharedPreferences mSharedPreferences;
    private String deepLinkEndPoint;

    @Override
    public void viewIsReady() {
        mSharedPreferences = getView().getAppContext().getSharedPreferences("donkey", Context.MODE_PRIVATE);
        getView().initFacebook();
        openWebViewWhenUrlExist();
    }

    @Override
    public void setupDeepLink(Uri targetUri, String countryCode) {
        if (targetUri != null && targetUri.getEncodedPath() != null
                && targetUri.getEncodedPath().length() > 1) {
            String endPoint = targetUri.getEncodedPath().substring(1);
            String[] key = endPoint.split(Pattern.quote("$$"));
            if (key.length > 0) {
                getUrl(countryCode, key[0]);
            }
            if (key.length > 1) {
                deepLinkEndPoint = key[1];
            }
        }
    }

    @Override
    public void openWebViewWhenUrlExist() {
        String newUrl = mSharedPreferences.getString("new_url", "");
        if (!Objects.equals(newUrl, "")) {
            getView().initVebView(newUrl);
        } else {
            TelephonyManager tm = (TelephonyManager) getView().getAppContext().getSystemService(TELEPHONY_SERVICE);
            if (tm != null) {
                getUrl(tm.getNetworkCountryIso(), "");
            } else {
                getUrl("XX", "");
            }
        }
    }

    @Override
    public void cachingUrl(String url) {
        if (url != null) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString("new_url", url);
            editor.apply();
        }
    }

    @Override
    public void getUrl(String countryCode, String key) {
        RaOfBookApp.getApi().getUrl(countryCode, key).enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.isSuccessful() && response.code()
                        == HttpURLConnection.HTTP_OK && response.body() != null) {
                    List<String> urls = response.body();
                    StringBuilder url = new StringBuilder();
                    if (urls.size() > 0) {
                        url.append(urls.get(0));
                        url.append("&geta=");
                        url.append(getView().getAppContext().getPackageName());
                        url.append("&getb=");
                        url.append(deepLinkEndPoint);
                        getView().initVebView(url.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                Log.e("Failure", "onFailure: ", t);
            }
        });
    }
}
