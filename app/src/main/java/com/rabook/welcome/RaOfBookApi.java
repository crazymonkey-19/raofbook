package com.rabook.welcome;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.rabook.welcome.Constants.GET_WEB_URL;

public interface RaOfBookApi {

    @GET(GET_WEB_URL)
    Call<List<String>> getUrl(@Query("code") String countryCode,
                              @Query("deeplink_keyword") String keyword);
}
